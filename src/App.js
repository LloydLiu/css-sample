import './App.css';
import {Header} from "./Header/Header";
import planet from './Images/planet.png'
import spacecraft from './Images/spaceCraft.png'
import cloud1 from './Images/cloud1.png'
import cloud2 from './Images/cloud2.png'
import spaceStation from './Images/space station.png'
import rocket from './Images/rocket.png'
import planet2 from './Images/planet 2.png'
import planet3 from './Images/planet 3.png'
import galaxy from  './Images/galaxy.png'

function App() {
    return (
        <div className="backGround">
            <Header/>
            <img id="planet" src={planet}/>
            <img id="spaceStation" src={spaceStation}/>
            <img id="airship" src={spacecraft}/>
            <img  id="planet2" src={planet2}/>
            <img id="planet3" src={planet3}/>
            <img id="cloud1" src={cloud1}/>
            <img id="cloud2" src={cloud2}/>


        </div>
    );
}

export default App;
